﻿using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Helper
{
   public class GenericRepository<T, TContext> : IGenericRepository<T>
        where T : class
        where TContext : DbContext
    {
        public TContext context;

    public GenericRepository(TContext context)
    {
        this.context = context;
    }

    public IQueryable<T> GetAll()
    {
        return context.Set<T>();
    }

    public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
    {
        return context.Set<T>();
    }

    public async Task<T> GetAsync(int id)
    {
        return await context.Set<T>().FindAsync(id);
    }

    public async Task<T> GetAsync(Int16 id)
    {
        return await context.Set<T>().FindAsync(id);
    }

    public T Add(T t)
    {
        context.Set<T>().Add(t);
        return t;
    }

    public T Update(T t)
    {
        context.Set<T>().Update(t);
        return t;
    }

    public List<T> Update(List<T> t)
    {
        context.Set<T>().AddRange(t);
        return t;
    }

    public T Remove(T t)
    {
        context.Set<T>().Remove(t);
        return t;
    }

    public void Dispose()
    {
        throw new NotImplementedException();
    }

    public bool Save()
    {
        return (context.SaveChanges() >= 0);
    }

    public async Task<bool> SaveAsync()
    {
        return (await context.SaveChangesAsync() >= 0);
    }

    public T Find(Expression<Func<T, bool>> match)
    {
        throw new NotImplementedException();
    }

    public List<T> AddDetails(List<T> t)
    {
        context.Set<T>().AddRange(t);
        return t;
    }
}
}
