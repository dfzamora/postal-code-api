﻿using System;
using System.Collections.Generic;

#nullable disable

namespace postal_codes_api.Models
{
    public partial class PostalInformation
    {
        public int Id { get; set; }
        public string Department { get; set; }
        public string Municipality { get; set; }
        public string Postalcode { get; set; }
        public string Neighbourhood { get; set; }
    }
}
