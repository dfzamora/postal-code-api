using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Helper;
using Interfaces;
using Microsoft.AspNetCore.Http;
using postal_codes_api.Models;

namespace Services
{

    public class PostalService : IPostalService
    {
        public IGenericRepository<PostalInformation> _postalHelper { get; }


        public PostalService(IGenericRepository<PostalInformation> postalHelper)
        {
            _postalHelper = postalHelper;
        }

        public List<PostalInformation> GetPostalCodes(string postalCode)
        {
            IQueryable<PostalInformation> query = _postalHelper.GetAll();

            if (string.IsNullOrEmpty(postalCode))
            {
                return query.ToList();
            }
            else
            {
                return query.Where(w => w.Postalcode.StartsWith(postalCode)).ToList();
            }
        }
        public List<PostalInformation> GetAllPostalCodes()
        {
            return _postalHelper.GetAll().ToList();
        }
    }
}