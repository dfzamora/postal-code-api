using Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Api.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [ApiExplorerSettings(GroupName = "v1")]
    [Route("api/v1/[controller]", Name = "PostalCode")]
    public class PostalCodeController : ControllerBase
    {
        public IPostalService _postalService { get; }

        public PostalCodeController(IPostalService postalService)=>_postalService=postalService;

        [HttpGet("{postalCode}")]
        public ActionResult GetCode(string postalCode)
        {
            return Ok(_postalService.GetPostalCodes(postalCode));
        }

         [HttpGet]
        public ActionResult GetAll()
        {
            return Ok(_postalService.GetPostalCodes(""));
        }
    }
    }