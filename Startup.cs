using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Helper;
using Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using postal_codes_api.Database;
using postal_codes_api.Models;
using Services;
// dotnet ef dbcontext scaffold "Server=dbpostalcodes.c5xwhjae6wlb.ca-central-1.rds.amazonaws.com;Database=dbpostal;User=admin;Password=12345678;" Pomelo.EntityFrameworkCore.MySql -o Models -c PostalContext --context-dir Database -f
namespace postal_codes_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            services.AddDbContext<PostalContext>(
              o => o.UseSqlServer(Configuration.GetConnectionString("Database"))
          );
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "postal codes api", Version = "v1" });
            });

            AddScoped(services);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "postal codes api v1"));

            app.UseHttpsRedirection();

            UseCors(app);

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void UseCors(IApplicationBuilder app)
        {

            string[] host = Configuration.GetSection("AllowedOrigins:hosts").Get<String[]>();
            string[] methods = Configuration.GetSection("AllowedOrigins:methods").Get<String[]>();

            app.UseCors(builder =>
            {
                builder
                .WithOrigins(host)
                .WithMethods(methods)
                .AllowAnyHeader();
            });

        }

        private void AddScoped(IServiceCollection service)
        {
            service.AddScoped<IGenericRepository<PostalInformation>, GenericRepository<PostalInformation, PostalContext>>();
            service.AddScoped<IPostalService, PostalService>();

        }
    }
}
