﻿using postal_codes_api.Models;
using System.Collections.Generic;

namespace Interfaces
{
    public interface IPostalService
    {
        List<PostalInformation> GetPostalCodes(string postalCode);
    }
}
