﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Interfaces
{
  public  interface IGenericRepository<T> where T : class
    {
        T Add(T t);
        T Update(T t);
        T Remove(T t);
        void Dispose();
        IQueryable<T> GetAll();
        IQueryable<T> Where(Expression<Func<T, bool>> predicate);
        Task<T> GetAsync(int id);
        Task<T> GetAsync(Int16 id);
        bool Save();
        Task<bool> SaveAsync();
        List<T> AddDetails(List<T> t);
        List<T> Update(List<T> t);
    }
}
